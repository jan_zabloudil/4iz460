
import pandas as pd
import matplotlib.pyplot as plt
from cleverminer import cleverminer

v_inf = pd.read_csv('Vehicle_Information.csv',encoding='unicode_escape')
a_inf = pd.read_csv('Accident_Information.csv')
data = pd.merge(a_inf, v_inf, how = "inner", on="Accident_Index")
data_final=data[['1st_Road_Class','2nd_Road_Class','Accident_Severity','Carriageway_Hazards','Date','Day_of_Week','Junction_Control','Junction_Detail','Light_Conditions','Number_of_Casualties','Number_of_Vehicles','Police_Force','Road_Surface_Conditions','Road_Type','Special_Conditions_at_Site','Speed_limit','Time','Urban_or_Rural_Area','Weather_Conditions','Year_x','Age_Band_of_Driver','Age_of_Vehicle','Driver_Home_Area_Type','Driver_IMD_Decile','Engine_Capacity_.CC.','Hit_Object_in_Carriageway','Hit_Object_off_Carriageway','Journey_Purpose_of_Driver','Junction_Location','make','Propulsion_Code','Sex_of_Driver','Skidding_and_Overturning','Vehicle_Leaving_Carriageway','Vehicle_Manoeuvre','Vehicle_Type','Was_Vehicle_Left_Hand_Drive','X1st_Point_of_Impact'
]]

data_final

data_final['Date']= pd.to_datetime(data_final['Date'])
data_final['Time']= pd.to_datetime(data_final['Time'])
data_final['Hour']=data_final['Time'].dt.hour
data_final['Month']=data_final['Date'].dt.month
data_final['Objem']=data_final.loc[data_final['Vehicle_Type']=='Car','Engine_Capacity_.CC.']
data_final['Objem']=round(data_final['Objem']/1000,2)

mon_edge=[0,2,5,8,11,12]
mon_lab=['Winter','Spring','Summer','Autumn','Winter']
data_final['Time_of_year_kat']=pd.cut(data_final['Month'],bins=mon_edge,labels=mon_lab,ordered=False).astype(str)
plt.hist(data_final['Time_of_year_kat'])
plt.show()

day_edge=[-1,6,10,16,20,25]
day_lab=['Night','AM Peak','Midday','PM Peak','Evening']
data_final['Time_of_day_kat']=pd.cut(data_final['Hour'],bins=day_edge,labels=day_lab).astype(str)
plt.hist(data_final['Time_of_day_kat'])
plt.show()

age_edge=[-1,3,7,10,15,25,float('inf')]
age_lab=['0-3','3-7','7-10','10-15','15-25','25+']
data_final['Age_of_Vehicle_kat']=pd.cut(data_final['Age_of_Vehicle'],bins=age_edge,labels=age_lab).astype(str)
plt.hist(data_final['Age_of_Vehicle_kat'])
plt.show()

objem_edge=[-1,1.3,1.6,2.0,3.0,float('inf')]
objem_lab=['0-1,3','1,3-1,6','1,6-2,0','2,0-3,0','3,0+']
data_final['Objem_kat']=pd.cut(data_final['Objem'],bins=objem_edge,labels=objem_lab).astype(str)
plt.hist(data_final['Objem_kat'])
plt.show()

casu_edge=[0,1,float('inf')]
casu_lab=['1','>=2']
data_final['Number_of_Casualties_kat']=pd.cut(data_final['Number_of_Casualties'],bins=casu_edge,labels=casu_lab)
plt.hist(data_final['Number_of_Casualties_kat'])
plt.show()

veh_edge=[0,1,2,float('inf')]
veh_lab=['1','2','>2']
data_final['Number_of_Vehicles_kat']=pd.cut(data_final['Number_of_Vehicles'],bins=veh_edge,labels=veh_lab)
plt.hist(data_final['Number_of_Vehicles_kat'])
plt.show()

allowed_values = ['VAUXHALL','FORD','PEUGEOT','VOLKSWAGEN','RENAULT','TOYOTA','CITROEN','NISSAN','BMW','MERCEDES','HONDA','FIAT','AUDI','ROVER','SKODA','VOLVO','LAND ROVER','MAZDA','HYUNDAI','SEAT','MITSUBISHI','KIA','SUZUKI','MINI','JAGUAR','SAAB','MG','DAEWOO','LEXUS','CHEVROLET','ALFA ROMEO','SUBARU','CHRYSLER','SMART','DAIHATSU','JEEP','PORSCHE','PROTON']
data_final['Znacky'] = None  

for index, row in data_final.iterrows():
    if row['Vehicle_Type'] == 'Car' and row['make'] in allowed_values:
        data_final.at[index, 'Znacky'] = row['make']  
    else:
        data_final.at[index, 'Znacky'] = None  

    percentage = (index + 1) / len(data_final) * 100
    print(f"Row {index + 1}: {percentage:.2f}%")

data_final['Age_Band_of_Driver']

data_final.to_csv('final_data.csv')

data_final=pd.read_csv('final_data.csv')

data_final=data_final['1st_Road_Class', '2nd_Road_Class', 'Accident_Severity', 'Carriageway_Hazards', 'Day_of_Week', 
                    'Junction_Control', 'Junction_Detail', 'Light_Conditions',
                    'Police_Force', 'Road_Surface_Conditions', 'Road_Type', 'Special_Conditions_at_Site', 'Speed_limit', 
                    'Urban_or_Rural_Area', 'Weather_Conditions', 'Year_x', 'Age_Band_of_Driver', 'Driver_Home_Area_Type',
                    'Driver_IMD_Decile','Hit_Object_in_Carriageway', 'Hit_Object_off_Carriageway', 
                    'Journey_Purpose_of_Driver', 'Junction_Location', 'Propulsion_Code', 'Sex_of_Driver', 
                    'Skidding_and_Overturning', 'Vehicle_Leaving_Carriageway', 'Vehicle_Manoeuvre', 'Vehicle_Type', 
                    'Was_Vehicle_Left_Hand_Drive', 'X1st_Point_of_Impact', 'Hour', 'Month', 'Time_of_year_kat', 
                    'Time_of_day_kat', 'Age_of_Vehicle_kat', 'Objem_kat', 'Number_of_Casualties_kat', 'Number_of_Vehicles_kat', 'Znacky']

# 1. analytická otázka
# Použit CFMiner

data_final.columns.to_list()

clm_1 = cleverminer(df=data_final,target='Year_x',proc='CFMiner',
               quantifiers= {'S_Down':5, 'Base':50},
               cond ={
                    'attributes':[
                        {'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Driver_Home_Area_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Road_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Speed_limit', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Urban_or_Rural_Area', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Time_of_year_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                    ], 'minlen':1, 'maxlen':7, 'type':'con'}
               )


clm_1.print_data_definition()
clm_1.print_rulelist()
clm_1.print_rule(26)
clm_1.print_rule(232)
clm_1.print_rule(392)
clm_1.print_rule(393)
clm_1.print_rule(395)

# 2. analytická otázka
# Použit CFMiner

clm_2 = cleverminer(df=data_final,target='Objem_kat',proc='CFMiner',
               quantifiers= {'S_Up':3, 'Base':50},
               cond ={
                    'attributes':[
                        {'name': 'Age_Band_of_Driver', 'type': 'seq', 'minlen': 1, 'maxlen': 4},
                        {'name': 'Driver_Home_Area_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Driver_IMD_Decile', 'type': 'seq', 'minlen': 1, 'maxlen': 2},
                    ], 'minlen':1, 'maxlen':4, 'type':'con'}
               )


clm_2.print_rulelist()


for i in range(0,6):
    clm_2.print_rule(i)


# 3. analytická otázka
# Použit 4ft Miner

clm_3 = cleverminer(df=data_final,proc='4ftMiner',
               quantifiers= {'conf':0.3, 'Base':50},
               ante ={
                    'attributes':[
                        {'name': 'Weather_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Road_Surface_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Light_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Special_Conditions_at_Site', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':10, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Junction_Detail', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )

clm_3.print_rulelist()

# 4. analytická otázka
# Použit 4ft Miner

clm_4 = cleverminer(df=data_final,proc='4ftMiner',
               quantifiers= {'conf':0.3, 'Base':50},
               ante ={
                    'attributes':[
                        {'name': 'Day_of_Week', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Urban_or_Rural_Area', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Accident_Severity', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Age_of_Vehicle_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':10, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Age_Band_of_Driver', 'type': 'seq', 'minlen': 1, 'maxlen': 2}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )

clm_4.print_rulelist()

# 5. analytická otázka
# Použit 4ft Miner

clm_5 = cleverminer(df=data_final,proc='4ftMiner',
               quantifiers= {'conf':0.5, 'Base':50},
               ante ={
                    'attributes':[
                        {'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Age_of_Vehicle_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Driver_Home_Area_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Objem_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':5, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )

clm_5.print_rulelist()

# 6. analytická otázka
# Použit 4ft Miner

clm_6 = cleverminer(df=data_final,proc='4ftMiner',
               quantifiers= {'conf':0.1, 'Base':50},
               ante ={
                    'attributes':[
                        {'name': 'Light_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Road_Surface_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Road_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Speed_limit', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Weather_Conditions', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                    ], 'minlen':1, 'maxlen':5, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Accident_Severity', 'type': 'lcut', 'minlen': 1, 'maxlen': 2}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )

clm_6.print_rulelist()

# 7. analytická otázka
# Použít 4ft-Miner

clm_1 = cleverminer(df=df,proc='4ftMiner',
quantifiers= {'conf':0.1, 'Base':50},
ante ={
'attributes':[
{'name': '1st_Road_Class', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
], 'minlen':1, 'maxlen':1, 'type':'con'},
succ ={
'attributes':[
{'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
], 'minlen':1, 'maxlen':1, 'type':'con'},
cond ={
'attributes':[
{'name': 'Accident_Severity', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
], 'minlen':1, 'maxlen':1, 'type':'con'}
)

clm_1.print_rulelist()

# 8. analytická otázka
# Použit SD4ft Miner

df1=df.loc[:, ('Day_of_Week', 'Age_Band_of_Driver', 'Accident_Severity', 'Sex_of_Driver')]

clm = cleverminer(df=df1,proc='SD4ftMiner',
               quantifiers= {'Base1':100, 'Base2':100, 'Ratioconf' : 2.0},
               ante ={
                    'attributes':[
                        {'name': 'Day_of_Week', 'type': 'subset', 'minlen': 1, 'maxlen': 2},
                        {'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 2},
                    ], 'minlen':1, 'maxlen':2, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Accident_Severity', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               frst ={
                    'attributes':[
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               scnd ={
                    'attributes':[
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )


clm.print_summary()
clm.print_rulelist()
clm.print_rule(1)

# 9. analytická otázka
# Použit SD4ft Miner

clm_9 = cleverminer(df=data_final,proc='SD4ftMiner',
               quantifiers= {'Base1':20, 'Base2':20, 'Ratioconf' : 2.0},
               ante ={
                    'attributes':[
                        {'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Objem_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Age_of_Vehicle_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':3, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Accident_Severity', 'type': 'lcut', 'minlen': 1, 'maxlen': 2}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               frst ={
                    'attributes':[
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               scnd ={
                    'attributes':[
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )



clm_9.print_rulelist()

# 10. analytická otázka
# Použit SD4ft Miner

clm_10 = cleverminer(df=data_final,proc='SD4ftMiner',
               quantifiers= {'Base1':20, 'Base2':20, 'Ratioconf' : 2.0},
               ante ={
                    'attributes':[
                        {'name': 'Road_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Age_Band_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Journey_Purpose_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Sex_of_Driver', 'type': 'subset', 'minlen': 1, 'maxlen': 1},
                        {'name': 'Time_of_year_kat', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':5, 'type':'con'},
               succ ={
                    'attributes':[
                        {'name': 'Driver_Home_Area_Type', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               frst ={
                    'attributes':[
                        {'name': 'Urban_or_Rural_Area', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'},
               scnd ={
                    'attributes':[
                        {'name': 'Urban_or_Rural_Area', 'type': 'subset', 'minlen': 1, 'maxlen': 1}
                    ], 'minlen':1, 'maxlen':1, 'type':'con'}
               )

clm_10.print_summary()
clm_10.print_rulelist()

clm_1.print_summary()
clm_2.print_summary()
clm_3.print_summary()
clm_4.print_summary()
clm_5.print_summary()
clm_6.print_summary()
clm_9.print_summary()
clm_10.print_summary()
