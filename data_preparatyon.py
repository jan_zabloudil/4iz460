import pandas as pd
import matplotlib.pyplot as plt
from cleverminer import cleverminer


v_inf = pd.read_csv('Vehicle_Information.csv',encoding='unicode_escape')
a_inf = pd.read_csv('Accident_Information.csv')
data = pd.merge(a_inf, v_inf, how = "inner", on="Accident_Index")
data_final=data[['1st_Road_Class','2nd_Road_Class','Accident_Severity','Carriageway_Hazards','Date','Day_of_Week','Junction_Control','Junction_Detail','Light_Conditions','Number_of_Casualties','Number_of_Vehicles','Police_Force','Road_Surface_Conditions','Road_Type','Special_Conditions_at_Site','Speed_limit','Time','Urban_or_Rural_Area','Weather_Conditions','Year_x','Age_Band_of_Driver','Age_of_Vehicle','Driver_Home_Area_Type','Driver_IMD_Decile','Engine_Capacity_.CC.','Hit_Object_in_Carriageway','Hit_Object_off_Carriageway','Journey_Purpose_of_Driver','Junction_Location','make','Propulsion_Code','Sex_of_Driver','Skidding_and_Overturning','Vehicle_Leaving_Carriageway','Vehicle_Manoeuvre','Vehicle_Type','Was_Vehicle_Left_Hand_Drive','X1st_Point_of_Impact'
]]
print(data_final)


data_final['Date']= pd.to_datetime(data_final['Date'])
data_final['Time']= pd.to_datetime(data_final['Time'])
data_final['Hour']=data_final['Time'].dt.hour
data_final['Month']=data_final['Date'].dt.month
data_final['Objem']=data_final.loc[data_final['Vehicle_Type']=='Car','Engine_Capacity_.CC.']
data_final['Objem']=round(data_final['Objem']/1000,2)
print(data_final['Hour'])
#####
mon_edge=[0,2,5,8,11,12]
mon_lab=['Winter','Spring','Summer','Autumn','Winter']
data_final['Time_of_year_kat']=pd.cut(data_final['Month'],bins=mon_edge,labels=mon_lab,ordered=False).astype(str)
plt.hist(data_final['Time_of_year_kat'])
plt.show()
data_final['Month'].unique()


day_edge=[-1,6,10,16,20,25]
day_lab=['Night','AM Peak','Midday','PM Peak','Evening']
data_final['Time_of_day_kat']=pd.cut(data_final['Hour'],bins=day_edge,labels=day_lab).astype(str)
plt.hist(data_final['Time_of_day_kat'])
plt.show()


age_edge=[-1,3,7,10,15,25,float('inf')]
age_lab=['0-3','3-7','7-10','10-15','15-25','25+']
data_final['Age_of_Vehicle_kat']=pd.cut(data_final['Age_of_Vehicle'],bins=age_edge,labels=age_lab).astype(str)
plt.hist(data_final['Age_of_Vehicle_kat'])
plt.show()



objem_edge=[-1,1.3,1.6,2.0,3.0,float('inf')]
objem_lab=['0-1,3','1,3-1,6','1,6-2,0','2,0-3,0','3,0+']
data_final['Objem_kat']=pd.cut(data_final['Objem'],bins=objem_edge,labels=objem_lab).astype(str)
plt.hist(data_final['Objem_kat'])
plt.show()



casu_edge=[0,1,float('inf')]
casu_lab=['1','>=2']
data_final['Number_of_Casualties_kat']=pd.cut(data_final['Number_of_Casualties'],bins=casu_edge,labels=casu_lab)
plt.hist(data_final['Number_of_Casualties_kat'])
plt.show()

veh_edge=[0,1,2,float('inf')]
veh_lab=['1','2','>2']
data_final['Number_of_Vehicles_kat']=pd.cut(data_final['Number_of_Vehicles'],bins=veh_edge,labels=veh_lab)
plt.hist(data_final['Number_of_Vehicles_kat'])
plt.show()


allowed_values = ['VAUXHALL','FORD','PEUGEOT','VOLKSWAGEN','RENAULT','TOYOTA','CITROEN','NISSAN','BMW','MERCEDES','HONDA','FIAT','AUDI','ROVER','SKODA','VOLVO','LAND ROVER','MAZDA','HYUNDAI','SEAT','MITSUBISHI','KIA','SUZUKI','MINI','JAGUAR','SAAB','MG','DAEWOO','LEXUS','CHEVROLET','ALFA ROMEO','SUBARU','CHRYSLER','SMART','DAIHATSU','JEEP','PORSCHE','PROTON']
data_final['Znacky'] = None  

for index, row in data_final.iterrows():
    if row['Vehicle_Type'] == 'Car' and row['make'] in allowed_values:
        data_final.at[index, 'Znacky'] = row['make']  
    else:
        data_final.at[index, 'Znacky'] = None  

    percentage = (index + 1) / len(data_final) * 100
    print(f"Row {index + 1}: {percentage:.2f}%")

data_final['Znacky']

data_final.to_csv('data_final.csv', index=False)